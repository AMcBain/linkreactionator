import com.google.gson.Gson;

import controllers.Application;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import play.Play;
import play.jobs.Every;
import play.jobs.Job;

@Every("30min")
public class AutoSave extends Job
{
    public void doJob ()
    {
        try (Writer favorites = new FileWriter(Play.getFile("logs/favorites.json"));
            Writer likes = new FileWriter(Play.getFile("logs/likes.json")))
        {
            Gson gson = new Gson();
            gson.toJson(Application.favorites, favorites);
            gson.toJson(Application.likes, likes);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
