package controllers;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.LineMetrics;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;

import play.Logger;
import play.Play;
import play.mvc.Controller;
import play.mvc.Http;

public class Application extends Controller
{
    public static Set<String> favorites;
    public static Set<String> likes;

    private static BufferedImage heart = null;
    private static BufferedImage thumb = null;

    private static volatile byte[] favoriteBytes = null;
    private static volatile byte[] likedBytes = null;
    private static volatile String favoriteHash = null;
    private static volatile String likedHash = null;

    private static Color ltblue = new Color(159, 207, 255);
    private static Color dkblue = new Color(34, 68, 136);
    private static Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 30);
    private static BasicStroke stroke = new BasicStroke(5);
    private static Ellipse2D.Float circle = new Ellipse2D.Float(81, 2, 66, 66);

    static
    {
        favorites = new HashSet<String>();
        likes = new HashSet<String>();

        heart = new BufferedImage(150, 144, BufferedImage.TYPE_INT_ARGB);
        try (InputStream stream = Play.getVirtualFile("public/images/favorite.png").inputstream())
        {
            BufferedImage img = ImageIO.read(stream);
            heart.getGraphics().drawImage(img, 0, 16, null);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        favoriteBytes = saveImage(heart, -1);
        favoriteHash = Arrays.hashCode(favoriteBytes) + "";

        thumb = new BufferedImage(150, 144, BufferedImage.TYPE_INT_ARGB);
        try (InputStream stream = Play.getVirtualFile("public/images/like.png").inputstream())
        {
            BufferedImage img = ImageIO.read(stream);
            thumb.getGraphics().drawImage(img, 0, 16, null);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        likedBytes = saveImage(thumb, -1);
        likedHash = Arrays.hashCode(likedBytes) + "";
    }

    private static String getPerson ()
    {
        Http.Header header = request.headers.get("x-forwarded-for");
        return (header != null && header.values.size() > 0 ? header.values.get(0) : request.remoteAddress);
    }

    private static byte[] redrawImage (BufferedImage image, int buffer, int count)
    {
        if (count > 0)
        {
            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                    RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
            graphics.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON));

            String str = count + "";
            graphics.setPaint(dkblue);
            graphics.fill(circle);
            graphics.setPaint(ltblue);
            graphics.setStroke(stroke);
            graphics.draw(circle);
            graphics.setFont(font);
            graphics.setPaint(Color.WHITE);
            LineMetrics lm = font.getLineMetrics(str, graphics.getFontRenderContext());
            int width = graphics.getFontMetrics().stringWidth(str);
            graphics.drawString(str, 114 - width / 2, 65 - 19);
        }
        return saveImage(image, buffer);
    }

    private static byte[] saveImage (BufferedImage image, int buffer)
    {
        ByteArrayOutputStream bytes = buffer > 0 ? new ByteArrayOutputStream(buffer) : new ByteArrayOutputStream();
        try (ImageOutputStream output = new MemoryCacheImageOutputStream(bytes)) {
            ImageIO.write(image, "png", output);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return bytes.toByteArray();
    }

    public static void rebuild ()
    {
        synchronized (favorites)
        {
            favoriteBytes = redrawImage(heart, favorites.size(), favoriteBytes.length);
            favoriteHash = Arrays.hashCode(favoriteBytes) + "";
        }
        favoriteImage();
    }

    public static void favorite ()
    {
        synchronized (favorites)
        {
            favorites.add(getPerson());
            favoriteBytes = redrawImage(heart, favoriteBytes.length, favorites.size());
            favoriteHash = Arrays.hashCode(favoriteBytes) + "";
        }
        redirect("https://www.pouet.net/topic.php?which=" + Play.configuration.get("application.topic"));
    }

    public static void like ()
    {
        synchronized (likes)
        {
            likes.add(getPerson());
            likedBytes = redrawImage(thumb, likedBytes.length, likes.size());
            likedHash = Arrays.hashCode(likedBytes) + "";
        }
        redirect("https://www.pouet.net/topic.php?which=" + Play.configuration.get("application.topic"));
    }

    private static boolean matchETag(String currentTag)
    {
        Http.Header header = request.headers.get("If-None-Match");
        if (header == null)
        {
            header = request.headers.get("if-none-match");
        }
        if (header != null)
        {
            return currentTag.equals(header.value());
        }
        return false;
    }

    public static void favoriteImage ()
    {
        response.setHeader("ETag", "\"" + favoriteHash + "\"");
        if (matchETag(favoriteHash))
        {
             notModified();
        }
        renderBinary(new ByteArrayInputStream(favoriteBytes), "favorite.png", favoriteBytes.length, true);
    }

    public static void likedImage ()
    {
        response.setHeader("ETag", "\"" + likedHash + "\"");
        if (matchETag(likedHash))
        {
             notModified();
        }
        renderBinary(new ByteArrayInputStream(likedBytes), "like.png", likedBytes.length, true);
    }
}
